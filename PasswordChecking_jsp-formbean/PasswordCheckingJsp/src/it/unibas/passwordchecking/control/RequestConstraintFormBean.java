/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.control;

import it.unibas.passwordchecking.Costanti;
import it.unibas.passwordchecking.model.ConstraintsChain;
import it.unibas.passwordchecking.model.constraints.ConstraintContainUsername;
import it.unibas.passwordchecking.model.constraints.ConstraintLenghtPassword;
import it.unibas.passwordchecking.model.constraints.ConstraintLowerCaseCharatters;
import it.unibas.passwordchecking.model.constraints.ConstraintMinimumNumbar;
import it.unibas.passwordchecking.model.constraints.ConstraintMinimumSizeSpecialCharatter;
import it.unibas.passwordchecking.model.constraints.ConstraintUpperCharacter;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestConstraintFormBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestConstraintFormBean.class);

    private boolean newFormBean = true;
    private String containsUsername = "false";
    private String lenghtPassword = "0";
    private boolean checkedLenghtPass = false;
    private String minimumUpperChar = "0";
    private boolean checkedUpperChar = false;
    private String minimumLowerChar = "0";
    private boolean checkedColwerChar = false;
    private String containNumbarMimimun = "0";
    private boolean checkedNumbar = false;
    private String containsSpecialCharacter = "0";
    private boolean checkedSpecialChar = false;
    private boolean submitForm = false;
    private String errors = "";
    

    public boolean isCheckedLenghtPass() {
        return checkedLenghtPass;
    }

    public boolean isCheckedUpperChar() {
        return checkedUpperChar;
    }

    public boolean isCheckedColwerChar() {
        return checkedColwerChar;
    }

    public boolean isCheckedNumbar() {
        return checkedNumbar;
    }

    public boolean isCheckedSpecialChar() {
        return checkedSpecialChar;
    }

    public boolean isNewFormBean() {
        return newFormBean;
    }

    public boolean isSubmitForm() {
        return submitForm;
    }

    public void setSubmitForm(boolean submitForm) {
        this.submitForm = submitForm;
    }

    public String getContainsUsername() {
        return containsUsername;
    }

    public String getLenghtPassword() {
        return lenghtPassword;
    }

    public String getMinimumUpperChar() {
        return minimumUpperChar;
    }

    public String getMinimumLowerChar() {
        return minimumLowerChar;
    }

    public String getContainNumbarMimimun() {
        return containNumbarMimimun;
    }

    public String getContainsSpecialCharacter() {
        return containsSpecialCharacter;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }
    
    
    public List<Boolean> checkDataRequest(HttpServletRequest request) {
        if (request == null) {
            throw new IllegalArgumentException("Request is null");
        }
        newFormBean = false;
        List<Boolean> errors = new ArrayList<>();
        containsUsername = request.getParameter(Costanti.NAME_USERNAME_CONTAINS);

        errors.add(verificValue(Costanti.NAME_USERNAME_CONTAINS, containsUsername, true));

        lenghtPassword = request.getParameter(Costanti.NAME_LENGHT_PASSWORD);
        if (lenghtPassword != null) {
            checkedLenghtPass = (lenghtPassword.equals("0") && newFormBean) ? false : true;
        }

        errors.add(verificValue(Costanti.NAME_LENGHT_PASSWORD, lenghtPassword, false));

        minimumUpperChar = request.getParameter(Costanti.NAME_LENGHT_UPPER_CHAR);
        if (minimumUpperChar != null) {
            checkedUpperChar = (minimumUpperChar.equals("0") && newFormBean) ? false : true;
        }

        errors.add(verificValue(Costanti.NAME_LENGHT_UPPER_CHAR, minimumUpperChar, false));

        minimumLowerChar = request.getParameter(Costanti.NAME_LENGHT_LOWER_CHAR);
        if (minimumLowerChar != null) {
            checkedColwerChar = (minimumLowerChar.equals("0") && newFormBean) ? false : true;
        }

        errors.add(verificValue(Costanti.NAME_LENGHT_LOWER_CHAR, minimumLowerChar, false));

        containNumbarMimimun = request.getParameter(Costanti.NAME_LENGHT_NUMBAR);
        if (containNumbarMimimun != null) {
            checkedNumbar = (containNumbarMimimun.equals("0") && newFormBean) ? false : true;
        }

        errors.add(verificValue(Costanti.NAME_LENGHT_NUMBAR, containNumbarMimimun, false));

        containsSpecialCharacter = request.getParameter(Costanti.NAME_LENGHT_SPECIAL_CHAR);
        if (containsSpecialCharacter != null) {
            checkedSpecialChar = (containsSpecialCharacter.equals("0") && newFormBean) ? false : true;
        }

        errors.add(verificValue(Costanti.NAME_LENGHT_SPECIAL_CHAR, containsSpecialCharacter, false));

        reintValueNull();
        return errors;
    }

    private boolean verificValue(String nameInput, String valueString, Boolean isBoolean) {
        if (valueString == null) {
            return true;
        }
        try {
            if (isBoolean) {
                if (!valueString.toLowerCase().contains("false")) {
                    LOGGER.debug("Parset boolean value: " + valueString + " result is: " + Boolean.parseBoolean(valueString));
                    return Boolean.parseBoolean(valueString);
                } else {
                    LOGGER.debug("The value is not equal true");
                    return true;
                }
            }
            int value = Integer.parseInt(valueString);
            LOGGER.debug("Value parserization integer is: " + valueString);
            boolean isPositive = value >= 0;
            if (!isPositive) {
                createMessageError(nameInput, "continene un numero negativo");
            }
            return isPositive;
        } catch (Exception e) {
            createMessageError(nameInput, " non continene un numero");
            e.printStackTrace();
            return false;
        }
    }

    private void reintValueNull() {
        if (containsUsername == null) {
            containsUsername = "false";
        }
        if (lenghtPassword == null) {
            lenghtPassword = "0";
        }
        if (minimumUpperChar == null) {
            minimumUpperChar = "0";
        }
        if (minimumLowerChar == null) {
            minimumLowerChar = "0";
        }
        if (containNumbarMimimun == null) {
            containNumbarMimimun = "0";
        }
        if (containsSpecialCharacter == null) {
            containsSpecialCharacter = "0";
        }
    }

    public void createChain(ConstraintsChain chain) {
        if (containsUsername.equals("true")) {
            chain.addConstrain(new ConstraintContainUsername());
        }
        if (!lenghtPassword.equals("0")) {
            int value = convertStringToValue(lenghtPassword);
            chain.addConstrain(new ConstraintLenghtPassword(value));
        }
        if (!minimumUpperChar.equals("0")) {
            int value = convertStringToValue(minimumUpperChar);
            chain.addConstrain(new ConstraintUpperCharacter(value));
        }
        if (!minimumLowerChar.equals("0")) {
            int value = convertStringToValue(minimumLowerChar);
            chain.addConstrain(new ConstraintLowerCaseCharatters(value));
        }
        if (!containNumbarMimimun.equals("0")) {
            int value = convertStringToValue(containNumbarMimimun);
            chain.addConstrain(new ConstraintMinimumNumbar(value));
        }
        if (!containsSpecialCharacter.equals("0")) {
            int value = convertStringToValue(containsSpecialCharacter);
            chain.addConstrain(new ConstraintMinimumSizeSpecialCharatter(value));
        }
    }

    private int convertStringToValue(String string) {
        if (string == null) {
            throw new IllegalArgumentException("Argument null");
        }
        int value = Integer.parseInt(string);
        return value;
    }

    private void createMessageError(String value, String message) {
        if ((value == null || value.isEmpty()) && (message == null || message.isEmpty())) {
            LOGGER.error("Input function null");
            throw new IllegalArgumentException("Input null");
        }
        if (value.equals("wille_have_not_name")) {
            throw new IllegalArgumentException("Qualcosa e' andato stoto il campo 'continene il nome' non e' un booleano");
        } else if (value.equals("lengh_pass_input")) {
            LOGGER.debug("Segnalo in errors l'errore per la lunghezza password");
            errors += "Il campo per la lunghezza della password " + message + ";";
        } else if (value.equals("lengh_numbar_upper_char")) {
            LOGGER.debug("Segnalo in errors l'errore per numero di caratter maiuscoli");
            errors += "Il campo per i caratteri maiuscoli " + message + ";";
        } else if (value.equals("lengh_numbar_lower_char")) {
            LOGGER.debug("Segnalo in errors l'errore per numero di caratter minuscoli");
            errors += "Il campo per i caratteri minuscoli " + message + ";";
        } else if (value.equals("lengh_numbar")) {
            LOGGER.debug("Segnalo in errors l'errore per numero di numeri");
            errors += "Il campo per i numeri contenuti " + message + ";";
        } else if (value.equals("lengh_special_char")) {
            LOGGER.debug("Segnalo in errors l'errore per il numero di caratteri speciali");
            errors += "Il campo per i caratteri speciali " + message;
        }

    }
}
