/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
 * California 94305, USA.
 */
package it.unibas.passwordchecking.control;

import it.unibas.passwordchecking.model.ContextMessage;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Vincenzo Palazzo
 */
public class LoginFormBean {

    private String user;
    private String password;

    public LoginFormBean(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public LoginFormBean() {
    }

    public List<String> checkError(HttpServletRequest request) {
        if(request == null){
            throw new IllegalArgumentException("input function null");
        }
        List<String> error = new ArrayList<>();

        String userPar = request.getParameter("username_value");
        if(userPar == null || userPar.isEmpty()){
            error.add("User mancante");
        }
        String passwordPar = request.getParameter("pass_value");
        if(passwordPar == null || passwordPar.isEmpty()){
            error.add("Password mancante");
        }

        if(!error.isEmpty()){
            return error;
        }
        this.user = userPar;
        this.password = passwordPar;

        return error;
    }

    public Object fromFormBeanToRealObjext(){
        if(user == null || password == null){
            throw new IllegalArgumentException("Password and/or user null");
        }
        return new ContextMessage(user, password);
    }

}
