/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking;

/**
 *
 * @author Vincenzo Palazzo
 */
public class Costanti {

   public static final String NAME_USERNAME_CONTAINS = "wille_have_not_name";
   public static final String NAME_LENGHT_PASSWORD = "lengh_pass_input";
   public static final String NAME_LENGHT_UPPER_CHAR = "lengh_numbar_upper_char";
   public static final String NAME_LENGHT_LOWER_CHAR = "lengh_numbar_lower_char";
   public static final String NAME_LENGHT_NUMBAR = "lengh_numbar";
   public static final String NAME_LENGHT_SPECIAL_CHAR = "lengh_special_char";

   public static final String[] SPECIAL_CHARATTER = {
        "!", "-", "_", "$", "£", "&", "?", "%", ".", ":", ";", ",", "@"
    };
   
   public static final String KEY_ACTION_CONTEXT = "KEY_ACTION_CONTEXT";
   public static final String FORWORD_ACTION_KEY = "KEY_FORWORD_ACTION";
   public static final String HOME_ACTION_KEY = "HOME_ACTION_KEY";
}
