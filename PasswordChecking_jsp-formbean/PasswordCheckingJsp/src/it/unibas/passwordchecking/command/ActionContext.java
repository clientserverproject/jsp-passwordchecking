/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.command;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ActionContext {
    
    private String action;
    private String url;

    public ActionContext(String action, String url) {
        this.action = action;
        this.url = url;
    }

    public ActionContext() {
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
}
