/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
 * California 94305, USA.
 */
package it.unibas.passwordchecking.command;

import it.unibas.passwordchecking.Costanti;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Vincenzo Palazzo
 */
public class CommandGoTo implements ICommandAction {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandGoTo.class);

    @Override
    public void doCommand(HttpServletRequest request, HttpServletResponse respose) {
        if (request == null || respose == null) {
            LOGGER.error("The input parametrer are null");
            throw new IllegalArgumentException("The input parameter are/is null");
        }
        HttpSession session = request.getSession(false);
        if (session == null) {
            throw new IllegalArgumentException("La sessione è scaduta oppure non e' mai iniziata");
        }
        ActionContext actionContext = (ActionContext) session.getAttribute(Costanti.KEY_ACTION_CONTEXT);
        if (actionContext == null) {
            LOGGER.error("ActionContext nullo non dovresti essere qui");
            throw new IllegalArgumentException("ActionContext nullo, non dovresti essere qui");
        }
        //session.removeAttribute(Constanti.KEY_ACTION_CONTEXT);
        session.invalidate();
        String url = actionContext.getUrl();
        LOGGER.debug("L'url e' ActionContext is: " + url);
        try {
            //TODO rifinire questa classe
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(url);
            try {
              requestDispatcher.forward(request, respose);
            } catch (ServletException ex) {
              LOGGER.error("ServletException: " + ex.getLocalizedMessage());
            }
            //respose.sendRedirect(url);
        } catch (IOException ex) {
            LOGGER.error("IOException: " + ex.getLocalizedMessage());
        }
    }

}
