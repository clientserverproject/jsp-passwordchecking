/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.command;

import java.io.IOException;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class CommandHome implements ICommandAction{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandHome.class);

    @Override
    public void doCommand(HttpServletRequest request, HttpServletResponse respose) {
        if(request == null || respose == null){
            LOGGER.debug("Input Argument are null");
            throw new IllegalArgumentException("Input Argument null");
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
        try {
            dispatcher.forward(request, respose);
        } catch (ServletException ex) {
            LOGGER.error("Exception generated is: " + ex.getLocalizedMessage());
        } catch (IOException ex) {
            LOGGER.error("Exception generated is: " + ex.getLocalizedMessage());
        }
    }
    
}
