/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
 * California 94305, USA.
 */
package it.unibas.passwordchecking.servlet;

import it.unibas.passwordchecking.Costanti;
import it.unibas.passwordchecking.command.ActionContext;
import it.unibas.passwordchecking.command.CommandGoTo;
import it.unibas.passwordchecking.command.CommandHome;
import it.unibas.passwordchecking.command.ICommandAction;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class MedietorServletActions extends HttpServlet{

    private static final Logger LOGGER = LoggerFactory.getLogger(MedietorServletActions.class);

    private Map<String, ICommandAction> medietor = new HashMap<>();

    public MedietorServletActions() {
        medietor.put(Costanti.FORWORD_ACTION_KEY, new CommandGoTo());
        medietor.put(Costanti.HOME_ACTION_KEY, new CommandHome());
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req == null || resp == null){
            LOGGER.error("Parametri input nulli");
            throw new IllegalArgumentException("Parametri input nulli");
        }
        HttpSession session = req.getSession(false);
        if(session == null){
            throw new IllegalArgumentException("Sessione scaduta o mai esistita");
        }
        ActionContext actionContext = (ActionContext) session.getAttribute(Costanti.KEY_ACTION_CONTEXT);
        if(actionContext == null){
            LOGGER.error("ActionContext e' nulla, non dovresti essere qui");
            throw new IllegalArgumentException("ActionContext e' nulla, non dovresti essere qui");
        }

        String action = actionContext.getAction();

        if(medietor.containsKey(action)){
            medietor.get(action).doCommand(req, resp);
            return;
        }

        //Vuol dire che non e' stato trovato il comando e questo in un applicazione
        //jsp deve essere sollavato tramite un eccezione perche' vuol dire che l'errore
        //e' stato del programmatore e va assolutamente corretto perche' queste azioni
        //possono essere usare per la navigazione tra schermi e quindi se non esiste il comando
        //esiste qualche errore durante la progettazione dello schermo
        throw new IllegalArgumentException("Comando non trovato");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }





}
