/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.model.constraints;

import it.unibas.passwordchecking.Costanti;
import it.unibas.passwordchecking.model.ContextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ConstraintUpperCharacter implements IConstraintHeadler{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ConstraintUpperCharacter.class);
    
    private int minimumUpperCaseCharatter;

    public ConstraintUpperCharacter(int minumimUpperCaseCharatter) {
        this.minimumUpperCaseCharatter = minumimUpperCaseCharatter;
        if(minumimUpperCaseCharatter < 0){
            throw new IllegalArgumentException("Numero minimo di caratteri maiuscoli negativo");
        }
    }

    public ConstraintUpperCharacter() {
        this(0);
    }
    
    

    @Override
    public boolean doConstrain(ContextMessage contextMessage) {
        if(contextMessage == null || contextMessage.hasComponetsNull()){
            LOGGER.error("The input argument is null");
            throw new IllegalArgumentException("Input nullo");
        }
        if(minimumUpperCaseCharatter == 0){
            return true;
        }
        String password = contextMessage.getPasswordInserited();
        int attualUpperCaseCharatter = 0;
        char[] value = password.toCharArray();
        
        for(Character c : value){
            if( Character.isUpperCase(c)){
                attualUpperCaseCharatter++;
            }
        }
        
        LOGGER.debug("Result post method is " + String.valueOf(attualUpperCaseCharatter < minimumUpperCaseCharatter));
        //TODO aggiungere  contextMessage.addError("Errore: La password contiene lo username");
        boolean flag = attualUpperCaseCharatter < minimumUpperCaseCharatter ? false : true; 
        if(!flag){
            contextMessage.addError("Errore: La password contiene meno di " + minimumUpperCaseCharatter + " caratteri maiuscoli");
        }
        return flag;
    }
    
    @Override
    public String toString() {
        return "Deve contenere minimo " + minimumUpperCaseCharatter + " caratteri maiuscoli";
    }
    
}
