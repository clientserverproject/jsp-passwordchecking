/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.model.constraints;

import it.unibas.passwordchecking.model.ContextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ConstraintContainUsername implements IConstraintHeadler{

    private static final Logger LOGGER = LoggerFactory.getLogger(ConstraintContainUsername.class);

    @Override
    public boolean doConstrain(ContextMessage contextMessage) {
        if(contextMessage == null || contextMessage.hasComponetsNull()){
            LOGGER.error("The input parameter is null");
            throw new IllegalArgumentException("Il paramentro d'input e' nullo");
        }

        String username = contextMessage.getUsername();

        boolean flag = contextMessage.getPasswordInserited().toUpperCase().contains(username.toUpperCase());

        if(flag){
            contextMessage.addError("Errore: La password contiene lo username");
        }

        return !flag;
    }

    @Override
    public String toString() {
        return "Non deve contenere l'username";
    }



}
