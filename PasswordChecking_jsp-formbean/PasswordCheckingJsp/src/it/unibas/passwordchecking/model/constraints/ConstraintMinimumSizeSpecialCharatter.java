/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.model.constraints;

import it.unibas.passwordchecking.Costanti;
import it.unibas.passwordchecking.model.ContextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ConstraintMinimumSizeSpecialCharatter implements IConstraintHeadler{

    private static final Logger LOGGER = LoggerFactory.getLogger(ConstraintMinimumSizeSpecialCharatter.class);
    
    private int minimumCharattersSpercial;

    public ConstraintMinimumSizeSpecialCharatter(int miminumCharatterSpercial) {
        if(miminumCharatterSpercial < 0){
            throw new IllegalArgumentException("Minimum numbar special charatter not valid");
        }
        this.minimumCharattersSpercial = miminumCharatterSpercial;
    }
    
    
    
    @Override
    public boolean doConstrain(ContextMessage contextMessage) {
        if(contextMessage == null || contextMessage.hasComponetsNull()){
            LOGGER.error("The input parameter is null");
            throw new IllegalArgumentException("Argument function null");
        }
        
        int counter = 0;
        
        String password = contextMessage.getPasswordInserited();
        char[] passwordToArray = password.toCharArray();
        
        for(String specialChar : Costanti.SPECIAL_CHARATTER){
            for(int i = 0; i < passwordToArray.length; i++){
                if(specialChar.equals(String.valueOf(passwordToArray[i]))){
                    counter++;
                }
            }
        }
        
        if(counter >= minimumCharattersSpercial){
            return true;
        }
        contextMessage.addError("Errore: La password contiene meno di " + minimumCharattersSpercial + " caratteri speciali");
        return false;
    }
    
    @Override
    public String toString() {
        return "Deve contenere minimo " + minimumCharattersSpercial + " caratteri speciali (?, _, !, ecc)";
    }
    
    
}
