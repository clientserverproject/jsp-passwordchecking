/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.model.constraints;

import it.unibas.passwordchecking.Costanti;
import it.unibas.passwordchecking.model.ContextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ConstraintMinimumNumbar implements IConstraintHeadler{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ConstraintMinimumNumbar.class);
    
    private int minimunNumbar;

    public ConstraintMinimumNumbar(int minimunNumbar) {
        if(minimunNumbar < 0){
            throw new IllegalArgumentException("Numbar insert not valid");
        }
        this.minimunNumbar = minimunNumbar;
    }
    
    

    @Override
    public boolean doConstrain(ContextMessage contextMessage) {
        if(contextMessage == null || contextMessage.hasComponetsNull()){
            LOGGER.debug("Argomenti della funzione nulli");
            throw new IllegalArgumentException("Argument null");
        }
        
        String passwordInsert = contextMessage.getPasswordInserited();
        LOGGER.debug("Password insert: " + passwordInsert);
        char[] passwordToArray = passwordInsert.toCharArray();
        
        int conter = 0;
        
        for(Character c : passwordToArray){
            LOGGER.debug("Numero analizzato: " + c);
            if(Character.isDigit(c)){
                conter++;
            }
           
        }
        
        
        if(conter >= minimunNumbar){
            return true;
        }
         contextMessage.addError("Errore: La password non ha un numero inferiore ad " + minimunNumbar + " caratteri");
        return false;
    }
    
    @Override
    public String toString() {
        return "Deve contenere minimo " + minimunNumbar + " numeri";
    }
    
}
