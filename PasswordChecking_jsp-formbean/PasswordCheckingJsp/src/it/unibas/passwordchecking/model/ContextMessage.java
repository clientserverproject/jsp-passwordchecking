/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ContextMessage {
    
    private String username;
    private String passwordInserited;
    private List<String> listErrors = new ArrayList<>();

    public ContextMessage(String username, String passwordInserited) {
        this.username = username;
        this.passwordInserited = passwordInserited;
    }

    public String getUsername() {
        return username;
    }

    public String getPasswordInserited() {
        return passwordInserited;
    }
    
    public void addError(String error){
        listErrors.add(error);
    }

    public List<String> getListErrors() {
        return listErrors;
    }
    
    public boolean hasErrors(){
        return !listErrors.isEmpty();
    }
    
    public boolean hasComponetsNull(){
        return (username == null) || (passwordInserited == null);
    }
    
}
