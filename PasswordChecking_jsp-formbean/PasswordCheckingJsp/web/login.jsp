<?xml version="1.0" encoding="utf-8"?>

<%@ page import="org.slf4j.Logger" %>
<%@ page import="org.slf4j.LoggerFactory" %>
<%@ page import="it.unibas.passwordchecking.model.constraints.*" %>
<%@ page import="it.unibas.passwordchecking.model.*" %>
<%@ page import="it.unibas.passwordchecking.control.*" %>
<%@ page import="it.unibas.passwordchecking.command.*" %>
<%@ page import="it.unibas.passwordchecking.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.IOException" %>

<%
    response.setHeader("Expires", "Thu, 1 Jan 1970 12:00:00 GMT");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");
%>

<%!
    //Metodo di servizio per fattorizzare il codice
    public void redirectJsp(String action, String page, PageContext pageContext, HttpSession contextSession) throws ServletException, IOException {
        if (page == null) {
            throw new IllegalArgumentException("L'input e' nullo");
        }

        ActionContext actionContext = new ActionContext(action, page);
        contextSession.setAttribute(action, actionContext);
        pageContext.forward("/index.action");
    }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.0//EN" "DTD-xhtmlbasic/xhtml-basic10.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">
    <head>
        <meta name="author" content="https://vincenzopalazzo.github.io/">
            <title>Password Cheker Unibas</title>
            <link rel="shortcut icon" href="icons/metaIcon.png"/>
            <link rel="stylesheet" href="css/primaryStyle.css" type="text/css"/>
            <link rel="stylesheet" href="css/materialInput.css" type="text/css"/>
            <link rel="stylesheet" href="css/buttons.css" type="text/css"/>
            <link rel="stylesheet" href="css/alter.css"/>
            <link rel="stylesheet" href="css/breadcrumb.css"/>
            <link rel="stylesheet" href="css/footer.css"/>
            <link rel="stylesheet" href="css/material-list-errors.css"/>
            <link rel="stylesheet" href="css/progressbar.css"/>
            <link rel="stylesheet" type="text/css" href="css/manualBootstrap.css"/>
            <script src="js/checkbox.js" charset="utf-8" type="text/javascript"></script>
            <script src="js/actions.js" type="text/javascript" charset="utf-8"></script>
            <script src="js/alter.js" charset="utf-8"></script>
            <script src="js/breadcrumb.js" type="text/javascript" charset="utf-8"></script>
            <script src="js/progressbar.js" type="text/javascript" charset="utf-8"></script>
    </head>

    <html>
        <body>

            <%
                Logger logger = LoggerFactory.getLogger("login.jsp");

                HttpSession contextSession = request.getSession(false);
                String pageBack = (String) contextSession.getAttribute("pageBack");

                if (contextSession == null || pageBack == null) {
                    logger.debug("Non e' stato possibile recuperare la sessione dell'utente, verra' indirizzato alla pagina di index");

                    redirectJsp(Costanti.HOME_ACTION_KEY, "/index.jsp", pageContext, contextSession);
                    return;
                }

                ActionContext actionContext = new ActionContext(Costanti.FORWORD_ACTION_KEY, "/index.jsp");
                contextSession.setAttribute(Costanti.KEY_ACTION_CONTEXT, actionContext);
                logger.debug("The action context is: " + contextSession.getAttribute(Costanti.KEY_ACTION_CONTEXT));
            %>

            <div aria-label="Breadcrumb" class="breadcrumb">
                <ol>
                    <li>
                        <a id="index" href="<%=response.encodeURL(pageBack + ".action")%>">
                            <img class="iconsBreadcrumb" src="icons/go-back-left-arrow.svg" alt="button back page"/>
                        </a>
                    </li>
                </ol>
            </div>

            <div id="alter-toast" role="alert"></div>
            <!--  The following script element contains the content that will be inserted into the alert element. -->
            <script type="text/template" id="alert-template">
                <div class="emoj" role="img" aria-label="star-struck">
                <p >&#x1F929;</p>
                <p id="message-alter">  Password OK<p>
                </div>
            </script>

            <div id="possword-toast" role="alert"></div>
            <!--  The following script element contains the content that will be inserted into the alert element. -->
            <script type="text/template" id="possword-template">

                <div class="emoj" role="img" aria-label="star-struck">
                <p >&#x1F635;</p>
                <p id="message-alter-possword">  Password K.O<p>
                </div>
            </script>

            <% //Inizio porzione di codece java

                RequestConstraintFormBean requestFormbean = (RequestConstraintFormBean) contextSession.getAttribute("controlRequest");
                List<Boolean> errors = (List<Boolean>) contextSession.getAttribute("errorsRequest");
                if (requestFormbean == null || errors.contains(Boolean.FALSE)) {
                    logger.debug("Non e' stato possibile recuperare la sessione dell'utente, verra' indirizzato alla pagina di index");

                    redirectJsp(Costanti.HOME_ACTION_KEY, "/index.jsp", pageContext, contextSession);
                }
                ContextMessage contextMessage = (ContextMessage) contextSession.getAttribute("contextMessage");

                StringTokenizer token = null;
                int nambarTrue = 0;
                int nambarConstraint = 0;
                List<Boolean> errorsChainValue = new ArrayList<Boolean>();

                if (contextMessage == null) {
                    //Sto provenendo da index.hmtl
                    logger.debug("Sto provenendo da index.html");
                    ContextMessage context = new ContextMessage(null, null); //Fittizio
                    ConstraintsChain chain = new ConstraintsChain();
                    requestFormbean.createChain(chain);
                    String constraintSetted = chain.toString();
                    token = new StringTokenizer(constraintSetted, ";");
                    contextSession.setAttribute("constraintToken", token);
                    logger.debug("Numbar constraints: " + token.countTokens());
                    contextSession.setAttribute("chain", chain);
                    contextSession.setAttribute("contextMessage", context);
                    Boolean firstVisit = true;
                    contextSession.setAttribute("firstVisit", firstVisit);
                    requestFormbean = null;

                }
                if (request.getParameter("is-submit") != null) {
                    //Ho effettuato il submit
                    Boolean firstVisit = false;
                    contextSession.setAttribute("firstVisit", firstVisit);
                    token = (StringTokenizer) contextSession.getAttribute("constraintToken");
                    logger.debug("Ho effettuato il submit della form login");
                    contextMessage = (ContextMessage) contextSession.getAttribute("contextMessage");
                    LoginFormBean loginFormBean = new LoginFormBean();
                    List<String> errorLogin = (List<String>) loginFormBean.checkError(request);
                    if (errorLogin.isEmpty()) {
                        contextMessage = (ContextMessage) loginFormBean.fromFormBeanToRealObjext();
                        ConstraintsChain chain = (ConstraintsChain) contextSession.getAttribute("chain");
                        //Prelevo i vincoli settati nella form precedente per visualizarli nella lista
                        String constraintSetted = chain.toString();
                        token = new StringTokenizer(constraintSetted, ";");

                        boolean flagResult = chain.doChain(contextMessage);
                        nambarConstraint = chain.getWhoFail().size();
                        /*
                      Devo clonare la lista altrimenti non riesco ad eseguire il caricamento della
                      progress bar correttamente oppure non riesco ad effetture l'operazione di
                      Checking password correttamente

                      Un esempio: Se effettuo un login scorretto e non svuoto la lista di errori
                      il successivo login risultera scorretto perche la lista di errori non e' ripulita
                      analogamente se la pulisco, il riferimento nambarConstraint verra svuotato perche
                      mantiene il riferimento della lista di errori all'interno del Chain e quindi
                      i cambiamenti nella lista originale verranno riflessi anche sui riferimenti che puntano
                      allo stesso oggetto nello heap.

                      Scrivendo new ArrayList<>(listaOriginale) evito di scrivere il clone su ogni riferimento
                      per applicare la clonazione profonda altrimenti avverrebbe una clonazione superficiale
                      per avere una clonazione profonda con questo frammento di codice originalList.clone();
                         */
                        errorsChainValue = new ArrayList<Boolean>(chain.getWhoFail());

                        if (flagResult) {
                            //Messaggio di conferma
                            logger.debug("Password O.K");
                            requestFormbean = null;
                            loginFormBean = null;
                            errorLogin = null;
                            contextMessage = null;
                %>
                <script type="text/javascript">
                        console.log("Message OK, run alter");
                        addAlertLoginOK();
                        saveError(-1);
                </script>
                <%
                } else {
                    //Messaggio di errore
                    logger.debug("Password K.O");
                    requestFormbean = null;
                    loginFormBean = null;
                    errorLogin = null;
                    contextMessage = null;
                    for (int i = 0; i < chain.getWhoFail().size(); i++) {
                        if (!chain.getWhoFail().get(i)) {
                %>
                <script type="text/javascript">
                    saveError("<%=i%>");
                </script>
                <%
                        }
                    }
                %>
                <script type="text/javascript">
                    console.log("Message K.O, run alter");
                    addAlertPasswordNotValid();
                </script>
                <%
                        }
                        chain.getWhoFail().clear();
                    } else {
                        String errorTmp = errorLogin.get(0);

                        if (errorTmp.contains("User")) {
                            String userError = "Username mancante";
                            contextSession.setAttribute("userError", userError);
                        } else if (errorTmp.contains("Password")) {
                            String passwordError = "Password mancante";
                            contextSession.setAttribute("passwordError", passwordError);
                        }

                        if (errorLogin.size() > 1) {
                            errorTmp = errorLogin.get(1);
                            if (errorTmp.contains("Password")) {
                                String passwordError = "Password mancante";
                                contextSession.setAttribute("passwordError", passwordError);
                            }
                        }
                    }

                    for (Boolean b : errorsChainValue) {
                        if (b == true) {
                            nambarTrue++;
                        }
                    }

                }
                Boolean firstVisit = (Boolean) contextSession.getAttribute("firstVisit");
                //Fine porzione di codece java
            %>

            <div class="container">

                <div class="inline-container">

                    <div class="material-list-wrapper">
                        <div class="mat_list_title">
                            <span id="percentual_id" class="percentual">Vincoli 0%</span>
                            <div id="raw_progress" class="progress">
                                <div id="progress_id" class="determinate"></div>
                            </div>
                        </div>
                        <ul class="mat_list">
                            <%
                                int i = 0;
                                while (token != null && token.hasMoreTokens()) {
                                    String nexToken = token.nextToken();
                                    logger.debug("Next token is " + nexToken);
                            %>
                            <li class="constraint" tabindex="<%=i%>">
                                <p><%=nexToken%></p>
                            </li>
                            <%
                                i++;
                            }%>
                        </ul>
                    </div>

                    <div class="block-container">

                        <h3 class="title-container">Inserisci le credenziali</h3>

                        <form class="form_login" id="login_form" action="<%=response.encodeURL("login.jsp")%>" method="post">

                            <div class="group">
                                <input class="material-inputs" id="username-input" name="username_value" type="text" required/>
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label for="pass_value">Username</label>
                                <%
                                    String userError = (String) contextSession.getAttribute("userError");
                                    if ((firstVisit != null && !firstVisit) && userError != null) {
                                %> 

                                <div class="error-message"><%=userError%></div>
                                <%
                                        contextSession.removeAttribute("userError");
                                    }
                                %>
                            </div>
                            <div class="group">
                                <input class="material-inputs" id="pass-input" name="pass_value" type="password" required/>
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label for="pass_value">Password</label>
                                <%
                                    String passError = (String) contextSession.getAttribute("passwordError");
                                    if ((firstVisit != null) && !firstVisit && (passError != null)) {
                                        %> 

                                            <div class="error-message"><%=passError%></div>
                                        <%
                                        contextSession.removeAttribute("passwordError");
                                    }
                                %>
                            </div>

                            <!--
                            Un altro modo per dichiare un bottone con WAI-ARIA ma questo modo non permette massima accessibilità
                            da tastiera quindi l'attributo role è applicabile anche ad un input
                            <span class="button_login" onclick="loginFormAriaButton()">
                                <div role="button" id="button_login" aria-controls="notes">
                                  Login
                                </div>
                              </span>
                            -->
                            <input type="hidden" name="is-submit" value="hidden"/>
                            <input class="button_login" type="submit" name="button_constraint" id="button_login"
                                   role="button" aria-controls="notes" onclick="avascript:loginFormAriaButton()" onsubmit="avascript:loginFormAriaButton()"
                                   value="Verifica"/>
                    </div>
                    </form>
                </div>
            </div>
            </div>

            </div>

            <%
                //Boolean firstVisit = (Boolean) contextSession.getAttribute("firstVisit");
                if (firstVisit != null && !firstVisit) {
            %>
            <script type="text/javascript">
                doProgress(<%=nambarConstraint%>, <%=nambarTrue%>);
            </script>
            <%
                }
            %>

            <script type="text/javascript">
                findErrors();
            </script>

            <%pageContext.include("footer.html");%>

        </body>
    </html>


    <!--
    /**
     * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
     * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
     * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo
     * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.
    
     * This work is licensed under the Creative Commons Attribution-ShareAlike License.
     * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/
     * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
     * California 94305, USA.
     */
    -->
