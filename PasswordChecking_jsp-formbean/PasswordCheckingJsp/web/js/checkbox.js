/*
*   This content is licensed according to the W3C Software License at
*   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
*   @Author https://vincenzopalazzo.github.io/
*   File:   Checkbox.js
*
*   Desc:   Checkbox widget that implements ARIA Authoring Practices
*           for a menu of links
*/

/*
*   @constructor Checkbox
*/
var Checkbox = function (domNode) {

  this.domNode = domNode;

  this.keyCode = Object.freeze({
    'RETURN': 13,
    'SPACE': 32
  });
  this.init();
};

Checkbox.prototype.init = function () {
  console.log("method init checkbox");

  if (!this.domNode.getAttribute('aria-checked')) {
    this.domNode.setAttribute('aria-checked', 'false');
  }

  this.domNode.addEventListener('keydown',    this.handleKeydown.bind(this));
  this.domNode.addEventListener('click',      this.handleClick.bind(this));
  this.domNode.addEventListener('focus',      this.handleFocus.bind(this));
  this.domNode.addEventListener('blur',       this.handleBlur.bind(this));

  //Added vincent
  this.disableEnableText(this.domNode);

};

Checkbox.prototype.toggleCheckbox = function () {
  console.log("Method toggleCheckbox");
  if (this.domNode.getAttribute('aria-checked') === 'true') {
    this.domNode.setAttribute('aria-checked', 'false');
    /*Added vincent*/
    this.disableEnableText(this.domNode);
  }else {
    this.domNode.setAttribute('aria-checked', 'true');
    /*Added vincent*/
    this.disableEnableText(this.domNode);
  }

};

/* EVENT HANDLERS */

Checkbox.prototype.handleKeydown = function (event) {
  var flag = false;

  switch (event.keyCode) {
    case this.keyCode.SPACE:
      this.toggleCheckbox();
      flag = true;
      break;

    default:
      break;
  }

  if (flag) {
    event.stopPropagation();
    event.preventDefault();
  }
};

Checkbox.prototype.handleClick = function (event) {
  console.log("method headleClick che");
  this.toggleCheckbox();
};

Checkbox.prototype.handleFocus = function (event) {
  this.domNode.classList.add('focus');
};

Checkbox.prototype.handleBlur = function (event) {
  this.domNode.classList.remove('focus');
};

/*
  This method disable or enable text input corripondent
*/

Checkbox.prototype.disableEnableText = function (checkBoxNode) {
  console.log("Call method disableEnableText");
  var index = checkBoxNode.getAttribute("tabindex");
  console.log("Index checkbox is: " + index);
  if(index === '1'){
    //Lenght pass input text
    var inputLenght = document.getElementById("ex1-input");
    this.toggleCheckboxOnClick(inputLenght);
  }else if(index === '2'){
    //Lenght Upper char
    var inputLenght = document.getElementById("min-upper-input");
    this.toggleCheckboxOnClick(inputLenght);
  }else if(index === '3'){
    //numbar lower chr
    var inputLenght = document.getElementById("min-lower-input");
    this.toggleCheckboxOnClick(inputLenght);
  }else if(index === '4'){
    // numbar contains
    var inputLenght = document.getElementById("numbar-input");
    this.toggleCheckboxOnClick(inputLenght);
  }else if(index === '5'){
    //Special char
    var inputLenght = document.getElementById("special-input");
    this.toggleCheckboxOnClick(inputLenght);
  }
};

Checkbox.prototype.toggleCheckboxOnClick = function (inputLenght) {
  console.log("Call method toggleCheckboxOnClick");
  if (this.domNode.getAttribute('aria-checked') === 'true') {
    /*Added vincent*/
    inputLenght.disabled = false;
    inputLenght.focus();
    console.log("The input is enabled?");
  }else {
    /*Added vincent*/
    inputLenght.disabled = true;
    console.log("The input is disabled?");
  }

};
