/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */

/*
  Questa azione crea la query string per la form
  dove vengono immesse i criteri che deve rispettare la password
*/

//TODO gestire i dati una volta ritornati nella form

function createUrlConstraint(){
  var checkBoxUsername = document.getElementById("aria_checkbox_username");
  console.log("Username is chacked? " + checkBoxUsername.getAttribute("aria-checked"));

  //require form
  var form_constraint = document.getElementById("form_constraint");

  /*
    Analizza la lo stato della checkbox per il contenimento nella password dell username BUG
  */
  if(document.getElementById("aria_checkbox_username").getAttribute("aria-checked") == 'true'){
      var textHtml = document.createElement("input");
      textHtml.setAttribute("type", "hidden");
      textHtml.setAttribute("name", "wille_have_not_name");
      textHtml.setAttribute("value", "true");
      form_constraint.appendChild(textHtml);
      console.log("Create and append input for chackbox");
  }

}

function loginFormAriaButton(){
  //require form
  console.log("login form whit a Aria button");
  document.getElementById("login_form").submit();
}

function submitFormWhitAriaButton(){
  //require form
  console.log("Submit form whit a Aria button");
  this.createUrlConstraint();
  document.getElementById("form_constraint").submit();
}

var errors = [];

function saveError(valueIdex){
  if(valueIdex == -1){
    console.log("The password is ok");
    return;
  }
  console.log("Pushed value " + valueIdex + " into errors");
  errors.push(valueIdex + "");

}

function findErrors(){
  var itesmList = document.getElementsByClassName("constraint");
  console.log("Element finded: " + itesmList.length);
  console.log("Error dimension is " + errors.length);
  for(i = 0; i < errors.length; i++){
    console.log("The error at the tabIndex: " + errors[i]);
    var tabIndex = errors[i];
    itesmList[tabIndex].style.backgroundColor = 'rgb(' + 251 + ',' + 157 + ',' + 157 + ')'; //251, 157, 157
  }
}

// Rendi la lista degl'errori visibile nella pagaina index.jsp
function setVisible(valueBoolean) {
  var divForList = document.getElementById("material_constraints_list");
  if(valueBoolean == true){
    divForList.style.visibility = 'visible';
    return;
  }
  divForList.style.visibility = 'hidden';
}
