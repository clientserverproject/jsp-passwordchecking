/*
*   This content is licensed according to the W3C Software License at
*   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
*   @Author https://vincenzopalazzo.github.io/
*   File:   Checkbox.js
*
*   Desc:   Checkbox widget that implements ARIA Authoring Practices
*           for a menu of links
*/

/*
 * @function addAlert
 *
 * @desc Adds an alert to the page
 *
 * @param   {Object}  event  -  Standard W3C event object
 *
 */

var alter;

function addAlertLoginOK() {
  console.log("Generate alter");
  alter = document.getElementById('alter-toast');
  var template = document.getElementById('alert-template').innerHTML;

  alter.innerHTML = template;

  window.setTimeout('closeAlter(alter)', 5000);

}

function addAlertPasswordNotValid() {
  console.log("Generate alter");
  alter = document.getElementById('possword-toast');
  var template = document.getElementById('possword-template').innerHTML;

  alter.innerHTML = template;
  window.setTimeout('closeAlter(alter)', 5000);

}


function closeAlter() {
  console.log("Closing alter")
  alter.style.display = "none";
}
