/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
 * California 94305, USA.
 */

var min = 0;
var progeressBar;
var stringConstraint = "Vincoli";

function doProgress(max, attualValue) {
  console.log("Metodo doProgress");
  console.log("Valore massimo progress bar: " + max);
  console.log("Valore progressione progress bar: " + attualValue);
  progeressBar = document.getElementById("progress_id");
  var percentual = document.getElementById("percentual_id");

  if(attualValue == max){
    console.log("Valore massimo");
    percentual.innerHTML = stringConstraint + " 100%";
    progeressBar.style.width = "100%";
    return;
  }

  if(attualValue == min){
    console.log("Valore minimo");
    percentual.innerHTML = stringConstraint + " 0%";
    progeressBar.style.width = "0%";
    return;
  }
  var difference = max - attualValue;
  var setValue = max - difference;
  
  if(setValue == 2 || setValue == (max/2)){
    console.log("Valore 50%");
    percentual.innerHTML = stringConstraint + " 50%";
    progeressBar.style.width = "50%";
    return;
  }
  
  if(setValue == 1){
    console.log("Valore 25%");
    percentual.innerHTML = stringConstraint + " 25%";
    progeressBar.style.width = "25%";
    return;
  }

  if(setValue == 3){
    console.log("Valore 75%");
    percentual.innerHTML = stringConstraint + " 75%";
    progeressBar.style.width = "75%";
    return;
  }

  if(setValue == 4){
    console.log("Valore 90%");
    percentual.innerHTML = stringConstraint + " 90%";
    progeressBar.style.width = "90%";
    return;
  }

}
