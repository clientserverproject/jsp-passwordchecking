<?xml version="1.0" encoding="UTF-8"?>

<%@ page import="it.unibas.passwordchecking.control.*" %>
<%@ page import="it.unibas.passwordchecking.model.*" %>
<%@ page import="it.unibas.passwordchecking.command.*" %>
<%@ page import="it.unibas.passwordchecking.*" %>
<%@ page import="java.util.*" %>
<%@ page import="org.slf4j.Logger" %>
<%@ page import="org.slf4j.LoggerFactory" %>

    <%
        response.setHeader("Expires", "Thu, 1 Jan 1970 12:00:00 GMT");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
    %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+ARIA 1.0//EN" "http://www.w3.org/WAI/ARIA/schemata/xhtml-aria-1.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">
      <head>
        <meta name="author" content="Vincenzo Palazzo"/>
        <title>Password Cheker Unibas</title>
        <link rel="shortcut icon" href="icons/metaIcon.png"/>
        <link rel="stylesheet" href="css/primaryStyle.css" type="text/css"/>
        <link rel="stylesheet" href="css/checkbox.css"/>
        <link rel="stylesheet" href="css/buttons.css"/>
        <link rel="stylesheet" href="css/textfieldcb.css"/>
        <link rel="stylesheet" href="css/breadcrumb.css"/>
        <link rel="stylesheet" href="css/footer.css"/>
        <link rel="stylesheet" href="css/material-list-errors.css"/>
        <link rel="stylesheet" type="text/css" href="css/manualBootstrap.css"/>
        <script src="js/checkbox.js" type="text/javascript"></script>
        <script src="js/actions.js" type="text/javascript"></script>
        <script src="js/breadcrumb.js" type="text/javascript"></script>
      </head>
    <body>

      <div aria-label="Breadcrumb" class="breadcrumb">
        <ol>
          <li>
            <a href="<%=response.encodeURL("index.jsp.action")%>">
              <img class="iconsBreadcrumb" src="icons/home.svg" alt="Bottone home"/>
            </a>
          </li>
        </ol>
      </div>

    <%
        Logger logger = LoggerFactory.getLogger( "index.jsp" );

        HttpSession contextSession = request.getSession();
        RequestConstraintFormBean requestFormbean = (RequestConstraintFormBean)contextSession.getAttribute("controlRequest");
        List<Boolean> errors = (List<Boolean>)contextSession.getAttribute("errorsRequest");
        ActionContext actionContext = new ActionContext(Costanti.FORWORD_ACTION_KEY, "/index.jsp");
        contextSession.setAttribute(Costanti.KEY_ACTION_CONTEXT, actionContext);
        logger.debug("L'azione e': " + contextSession.getAttribute(Costanti.KEY_ACTION_CONTEXT));
        logger.debug("Il parametre della query string submit esiste? " + request.getParameter("is-submit"));
        if(requestFormbean == null){
            requestFormbean = new RequestConstraintFormBean();
            contextSession.setAttribute("controlRequest", requestFormbean);
        }
        if(request.getParameter("is-submit") != null){
            logger.debug("La lista degli errori e' null");
            errors = requestFormbean.checkDataRequest(request);
            logger.debug("la lista degli errori contiene errori? " + errors.contains(Boolean.FALSE));
            contextSession.setAttribute("errorsRequest", errors);
            String errorsSting = requestFormbean.getErrors();
            logger.debug("Gli errori sono: " + errorsSting);
            if(!errors.contains(Boolean.FALSE)){
              requestFormbean = null;
              String pageBack = response.encodeURL("index.jsp");
              contextSession.setAttribute("pageBack", pageBack);
              pageContext.forward("login.jsp");
              return;
            }
            if(!errorsSting.isEmpty()){
              logger.debug("errors String is: " + errorsSting);
              StringTokenizer token = new StringTokenizer(errorsSting, ";");
              contextSession.setAttribute("errorsConstraints", token);
              requestFormbean.setErrors("");
            }else{
              logger.debug("Errors strins empty");
            }
        }
    %>
    <div class="container">

      <div class="inline-container">

        <div id="material_constraints_list" class="material-list-wrapper">
          <div class="mat_list_title">
      				<span>Errori</span>
      		</div>
          <ul class="mat_list">
            <%
                StringTokenizer token = (StringTokenizer)contextSession.getAttribute("errorsConstraints");
                int i = 0;
                if(token == null || !token.hasMoreTokens()){
                  %>
                  <!-- Rendere visibile la schermata degli errori-->
                  <script type="text/javascript">
                    setVisible(false);
                  </script>
                  <%
                }
                while(token != null && token.hasMoreTokens()){
                  %>
                  <!-- Rendere visibile la scermata degli errori-->
                  <script type="text/javascript">
                    setVisible(true);
                  </script>
                  <%
                  String nexToken = token.nextToken();
                  logger.debug("Next token is " + nexToken);
                 %>
                 <li class="constraint" tabindex="<%=i%>">
                   <p><%=nexToken %></p>
                 </li>
                <%
                  i++;
                }
                %>
          </ul>
        </div>
        <div class="block-container">
          <h3 class="title-container">Scegli i criteri che la tua password deve rispettare</h3>
          <!-- Questa form viene sottomessa attraverso javascript -->
          <form class="form_constraint" id="form_constraint" action="index.jsp" method="post">

                <div class="constraint-position" role="group" aria-labelledby="id-group-label">

                  <ul class="checkboxes">
                    <li>
                      <div id="aria_checkbox_username" role="checkbox" aria-checked="<%= requestFormbean.getContainsUsername() %>" tabindex="0">
                        <label>Username</label>
                      </div>
                    </li>

                    <li>
                      <div id="aria_checkbox_lenght_pass" role="checkbox" aria-checked="<%= requestFormbean.isCheckedLenghtPass() %>" tabindex="1">
                        <label for="ex1-input">Lunghezza password</label>
                      </div>
                        <div class="combobox-wrapper">
                          <div role="combobox" aria-expanded="false" aria-owns="ex1-listbox"
                                id="ex1-combobox">
                            <input class="ln_c_dist" name="lengh_pass_input" type="text" id="ex1-input" value="<%= requestFormbean.getLenghtPassword() %>"/>
                          </div>
                        </div>
                    </li>

                    <li>
                      <div id="aria_checkbox_upper_charatter" role="checkbox" aria-checked="<%= requestFormbean.isCheckedUpperChar() %>" tabindex="2">
                        <label for="min-upper-input">Numero caratteri maiuscoli</label>
                      </div>
                        <div class="combobox-wrapper">
                          <div role="combobox" aria-expanded="false" aria-owns="ex1-listbox"
                                id="min-upper-combobox">
                            <input class="up_c_dist" name="lengh_numbar_upper_char" type="text" id="min-upper-input" value="<%= requestFormbean.getMinimumUpperChar() %>" />
                          </div>
                      </div>
                    </li>
                    <li>
                      <div id="aria_checkbox_lower_charatter" role="checkbox" aria-checked="<%= requestFormbean.isCheckedColwerChar() %>" tabindex="3">
                        <label for="min-lower-input">Numero caratteri minuscoli</label>
                      </div>
                        <div class="combobox-wrapper">
                          <div role="combobox" aria-expanded="false" aria-owns="ex1-listbox"
                                id="min-lower-combobox">
                            <input class="low_c_dist" name="lengh_numbar_lower_char" type="text" id="min-lower-input"
                               value="<%= requestFormbean.getMinimumLowerChar() %>" />
                          </div>
                        </div>
                    </li>

                    <li>
                      <div id="aria_checkbox_numbar" role="checkbox" aria-checked="<%= requestFormbean.isCheckedNumbar() %>" tabindex="4">
                        <label for="numbar-input">Contiene numeri</label>
                      </div>
                      <div class="combobox-wrapper">
                          <div role="combobox" aria-expanded="false" aria-owns="ex1-listbox"
                                id="numbar-combobox">
                            <input class="num_c_dist" name="lengh_numbar" type="text" id="numbar-input" value="<%= requestFormbean.getContainNumbarMimimun() %>" />
                          </div>
                        </div>
                    </li>

                    <li>
                      <div id="aria_checkbox_special_charatter" role="checkbox" aria-checked="<%= requestFormbean.isCheckedSpecialChar() %>" tabindex="5">
                        <label for="special-input">Caratteri Speciali</label>
                      </div>
                        <div class="combobox-wrapper">
                          <div role="combobox" aria-expanded="false" aria-owns="ex1-listbox"
                                id="special-combobox">
                            <input class="special_c_dist" name="lengh_special_char" id="special-input" value="<%= requestFormbean.getContainsSpecialCharacter() %>" />
                          </div>
                        </div>
                    </li>

                  </ul>
                  <!--
                  Un altro modo per dichiare un bottone con WAI-ARIA ma questo modo non permette massima accessibilità
                  da tastiera quindi l'attributo role è applicabile anche ad un input
                  <span class="button_submit" id="button_constraint" onclick="submitFormWhitAriaButton()">
                      <a role="button" id="button_submit_constraint">
                        Adding Constraints
                      </a>
                  </span>
                  -->
                <input type="hidden" name="is-submit" value="hidden"/>
                 <input id="button_submit_constraint"
                        class="button_submit"
                        type="submit"
                        name="button_constraint"
                        role="button"
                        aria-controls="notes"
                        onclick="submitFormWhitAriaButton()"
                        onsubmit="submitFormWhitAriaButton()"
                        value="Aggiungi"/>
                </div>
              </form>
          </div>
        </div>
      </div>
        <script type="text/javascript">
          window.onload = function () {

            var checkboxes = document.querySelectorAll('[role="checkbox"]');

            for (var i = 0; i < checkboxes.length; i++) {
              console.log("init checkbox");
              var cb = new Checkbox(checkboxes[i]);
            }

          };
        </script>
      <%pageContext.include("footer.html"); %>
  </body>
</html>

<!--
/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford,
 * California 94305, USA.
 */
 -->
