/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.model;

import it.unibas.passwordchecking.model.ConstraintsChain;
import it.unibas.passwordchecking.model.ContextMessage;
import it.unibas.passwordchecking.model.constraints.ConstraintContainUsername;
import it.unibas.passwordchecking.model.constraints.ConstraintLenghtPassword;
import it.unibas.passwordchecking.model.constraints.ConstraintLowerCaseCharatters;
import it.unibas.passwordchecking.model.constraints.ConstraintMinimumNumbar;
import it.unibas.passwordchecking.model.constraints.ConstraintMinimumSizeSpecialCharatter;
import it.unibas.passwordchecking.model.constraints.ConstraintUpperCharacter;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Vincenzo Palazzo
 */
public class ChainTest {
    
    private ConstraintsChain chain;
    
    @Before
    public void initDataTests(){
        chain = new ConstraintsChain();
        chain.addConstrain(new ConstraintContainUsername());
        chain.addConstrain(new ConstraintLenghtPassword(8));
        chain.addConstrain(new ConstraintMinimumNumbar(1));
        chain.addConstrain(new ConstraintMinimumSizeSpecialCharatter(1));
        chain.addConstrain(new ConstraintUpperCharacter(1));
        chain.addConstrain(new ConstraintLowerCaseCharatters(7));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDoChainConstraintNullOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", null);
        chain.doChain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDoChainConstraintNullTwo(){
        ContextMessage contextMessage = new ContextMessage(null, "dsfdasd");
        chain.doChain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDoChainConstraintNullThree(){
        ContextMessage contextMessage = null;
        chain.doChain(contextMessage);
    }
    
    @Test
    public void testDoChainConstraintOKOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "PippiCalzeLunge1995!");
        TestCase.assertTrue(chain.doChain(contextMessage));
    }
    
    @Test
    public void testDoChainConstraintKOOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "PippiCalzeLunge1995");
        TestCase.assertFalse(chain.doChain(contextMessage));
        TestCase.assertEquals(contextMessage.getListErrors().get(0), "Errore: La password contiene meno di 1 caratteri speciali");
    }
    
}
