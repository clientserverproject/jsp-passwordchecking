/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.model.constraints;

import it.unibas.passwordchecking.model.ConstraintsChain;
import it.unibas.passwordchecking.model.ContextMessage;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 * Questo test è stato scritto perché durante l'esecuzione dell'applicazione
 * Si era riscontrato un bug nell'inserimento di lettere e numeri insieme
 * Ora grazie a questa batteria di test il bug non esiste più
 * 
 * @author Vincenzo Palazzo
 */
public class HeaderContainsNumberConstraintTest {

    ConstraintsChain chain;

    @Before
    public void initDatas() {
        chain = new ConstraintsChain();
    }

    @Test
    public void testContrainOnlyNumbar() {
        chain.addConstrain(new ConstraintMinimumNumbar(12));
        ContextMessage message = new ContextMessage("Pippo", "123456789123");
        TestCase.assertTrue(chain.doChain(message));
    }
    
    @Test
    public void testContrainNumbarPluCharatterSpecialOne() {
        chain.addConstrain(new ConstraintMinimumNumbar(1));
        chain.addConstrain(new ConstraintMinimumSizeSpecialCharatter(1));
        ContextMessage message = new ContextMessage("Pippo", "1?");
        TestCase.assertTrue(chain.doChain(message));
    }
    
    @Test
    public void testContrainNumbarPluCharatterSpecialTwo() {
        chain.addConstrain(new ConstraintMinimumNumbar(1));
        chain.addConstrain(new ConstraintMinimumSizeSpecialCharatter(1));
        ContextMessage message = new ContextMessage("Pippo", "-?");
        TestCase.assertFalse(chain.doChain(message));
    }
    
    @Test
    public void testContrainNumbarPluCharatterSpecialThree() {
        chain.addConstrain(new ConstraintMinimumNumbar(1));
        chain.addConstrain(new ConstraintMinimumSizeSpecialCharatter(1));
        ContextMessage message = new ContextMessage("Pippo", "12");
        TestCase.assertFalse(chain.doChain(message));
    }
}
