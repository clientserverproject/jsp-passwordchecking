/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.model.constraints;

import it.unibas.passwordchecking.model.ContextMessage;
import it.unibas.passwordchecking.model.constraints.ConstraintLenghtPassword;
import it.unibas.passwordchecking.model.constraints.ConstraintLowerCaseCharatters;
import it.unibas.passwordchecking.model.constraints.ConstraintMinimumNumbar;
import it.unibas.passwordchecking.model.constraints.ConstraintMinimumSizeSpecialCharatter;
import it.unibas.passwordchecking.model.constraints.ConstraintUpperCharacter;
import it.unibas.passwordchecking.model.constraints.IConstraintHeadler;
import junit.framework.TestCase;
import org.junit.Test;

/**
 *
 * @author Vincenzo Palazzo
 */
public class SimpleHeadlerTest{
    
    //TODO mancano i test sugl'errori in ContextMessage
    
    private IConstraintHeadler constraint;
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintLenghtPasswordContextNullOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", null);
        constraint = new ConstraintLenghtPassword(8);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintLenghtPasswordContextNullTwo(){
        ContextMessage contextMessage = new ContextMessage(null, "the_username_is_null");
        constraint = new ConstraintLenghtPassword(8);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintLenghtPasswordContextNullThree(){
        ContextMessage contextMessage = null;
        constraint = new ConstraintLenghtPassword(8);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintLenghtPasswordContextNullFour(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "thiisatest");
        constraint = new ConstraintLenghtPassword(-8);
        constraint.doConstrain(contextMessage);
    }
    
    @Test
    public void testConstraintLenghtPasswordFalseOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "thiisatest");
        constraint = new ConstraintLenghtPassword(18);
        TestCase.assertFalse(constraint.doConstrain(contextMessage));
        TestCase.assertEquals(contextMessage.getListErrors().get(0), "Errore: La lunghezza della password è minore di 18 caratteri");
    }
    
    @Test
    public void testConstraintLenghtPasswordTrueOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "thiisatest");
        constraint = new ConstraintLenghtPassword(8);
        TestCase.assertTrue(constraint.doConstrain(contextMessage));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarUpperCaseCharatterNullOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", null);
        constraint = new ConstraintUpperCharacter(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarUpperCaseCharatterNullTwo(){
        ContextMessage contextMessage = new ContextMessage(null, "qwerty");
        constraint = new ConstraintUpperCharacter(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarUpperCaseCharatterNullThree(){
        ContextMessage contextMessage = null;
        constraint = new ConstraintUpperCharacter(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarUpperCaseCharatterNullTFour(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "PippicalzeLunge");
        constraint = new ConstraintUpperCharacter(-2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test
    public void testConstraintMinimumNumbarUpperCaseCharatterOOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "PippicalzeLunge");
        constraint = new ConstraintUpperCharacter(2);
        TestCase.assertTrue(constraint.doConstrain(contextMessage));
    }
    
    @Test
    public void testConstraintMinimumNumbarUpperCaseCharatterKoOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "Pippicalzelunge");
        constraint = new ConstraintUpperCharacter(2);
        TestCase.assertFalse(constraint.doConstrain(contextMessage));
        TestCase.assertEquals(contextMessage.getListErrors().get(0), "Errore: La password contiene meno di 2 caratteri maiuscoli");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarLowerCaseCharatterNullOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", null);
        constraint = new ConstraintLowerCaseCharatters(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarLowerCaseCharatterNullTwo(){
        ContextMessage contextMessage = new ContextMessage(null, "qwerty");
        constraint = new ConstraintLowerCaseCharatters(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarLowerCaseCharatterNullThree(){
        ContextMessage contextMessage = null;
        constraint = new ConstraintLowerCaseCharatters(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarLowerCaseCharatterNullTFour(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "PippicalzeLunge");
        constraint = new ConstraintLowerCaseCharatters(-2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test
    public void testConstraintMinimumNumbarLowerCaseCharatterOkne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "PippicalzeLungeeee");
        constraint = new ConstraintLowerCaseCharatters(16);
        TestCase.assertTrue(constraint.doConstrain(contextMessage));
    }
    
    @Test
    public void testConstraintMinimumNumbarLowerCaseCharatterKoOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "PippicalzeLungee");
        constraint = new ConstraintLowerCaseCharatters(18);
        TestCase.assertFalse(constraint.doConstrain(contextMessage));
        TestCase.assertEquals(contextMessage.getListErrors().get(0), "Errore: La password contiene meno di 18 caratteri minuscoli");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarNullOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", null);
        constraint = new ConstraintMinimumNumbar(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarNullTwo(){
        ContextMessage contextMessage = new ContextMessage(null, "qwerty");
        constraint = new ConstraintMinimumNumbar(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarNullThree(){
        ContextMessage contextMessage = null;
        constraint = new ConstraintMinimumNumbar(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumNumbarNullTFour(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "PippicalzeLungee");
        constraint = new ConstraintMinimumNumbar(-2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test
    public void testConstraintMinimumNumbarOKOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "Pippi1calzeL1ungee24");
        constraint = new ConstraintMinimumNumbar(4);
        TestCase.assertTrue(constraint.doConstrain(contextMessage));
    }
    
    @Test
    public void testConstraintMinimumNumbarKoOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "PippicalzeLungee2019");
        constraint = new ConstraintMinimumNumbar(5);
        TestCase.assertFalse(constraint.doConstrain(contextMessage));
        TestCase.assertEquals(contextMessage.getListErrors().get(0), "Errore: La password non ha un numero inferiore ad 5 caratteri");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumCaratterSpecialNullOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", null);
        constraint = new ConstraintMinimumSizeSpecialCharatter(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumCaratterSpecialNullTwo(){
        ContextMessage contextMessage = new ContextMessage(null, "qwerty");
        constraint = new ConstraintMinimumSizeSpecialCharatter(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumCaratterSpecialNullThree(){
        ContextMessage contextMessage = null;
        constraint = new ConstraintMinimumSizeSpecialCharatter(2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstraintMinimumCaratterSpecialNullTFour(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "PippicalzeLungee");
        constraint = new ConstraintMinimumSizeSpecialCharatter(-2);
        constraint.doConstrain(contextMessage);
    }
    
    @Test
    public void testConstraintMinimumCaratterSpecialOKOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "Pipp.calzr.lunghe&_");
        constraint = new ConstraintMinimumSizeSpecialCharatter(4);
        TestCase.assertTrue(constraint.doConstrain(contextMessage));
        TestCase.assertFalse(contextMessage.hasErrors());
    }
    
    @Test
    public void testConstraintMinimumCaratterSpecialKoOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "unibas_Laurea-2.01.9");
        constraint = new ConstraintMinimumSizeSpecialCharatter(5);
        TestCase.assertFalse(constraint.doConstrain(contextMessage));
        TestCase.assertTrue(contextMessage.hasErrors());
        TestCase.assertEquals(contextMessage.getListErrors().get(0), "Errore: La password contiene meno di 5 caratteri speciali");
    }
    
    @Test
    public void testConstraintUsernameOkOne(){
        ContextMessage contextMessage = new ContextMessage("vincenzopalazzo", "vincenzoCalze");
        constraint = new ConstraintContainUsername();
        TestCase.assertTrue(constraint.doConstrain(contextMessage));
        TestCase.assertFalse(contextMessage.hasErrors());
    }
    
    @Test
    public void testConstraintUsernameKOOne(){
        ContextMessage contextMessage = new ContextMessage("atarw", "atarw");
        constraint = new ConstraintContainUsername();
        TestCase.assertFalse(constraint.doConstrain(contextMessage));
        TestCase.assertTrue(contextMessage.hasErrors());
        TestCase.assertEquals(contextMessage.getListErrors().get(0), "Errore: La password contiene lo username");
    }
    
}
