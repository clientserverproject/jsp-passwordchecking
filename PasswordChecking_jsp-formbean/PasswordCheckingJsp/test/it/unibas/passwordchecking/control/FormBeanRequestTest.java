/**
 * Questo lavoro viene concesso in uso secondo i termini della licenza “Attribution-ShareAlike”
 * di Creative Commons. Per ottenere una copia della licenza, è possibile visitare
 * http://creativecommons.org/licenses/by-sa/1.0/ oppure inviare una lettera all’indirizzo 
 * Creative Commons, 559 Nathan Abbott Way, Stanford, California 94305, USA.

 * This work is licensed under the Creative Commons Attribution-ShareAlike License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/1.0/ 
 * or send a letter to Creative Commons, 559 Nathan Abbott Way, Stanford, 
 * California 94305, USA.
 */
package it.unibas.passwordchecking.control;

import it.unibas.passwordchecking.mock.HttpServletRequestMock;
import it.unibas.passwordchecking.control.*;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Vincenzo Palazzo
 */
public class FormBeanRequestTest {
    
    private RequestConstraintFormBean formBean;
    private HttpServletRequestMock mockRequest;
    
    @Before
    public void setUp() {
        formBean = new RequestConstraintFormBean();
    }
    
    @Test
    public void testFormBeanOne(){
        mockRequest = new HttpServletRequestMock("true", "2", "12", "-4", "2", null);
        List<Boolean> errors = formBean.checkDataRequest(mockRequest);
        TestCase.assertTrue(errors.contains(Boolean.FALSE));
    }
    
    @Test
    public void testFormBeanTwo(){
        mockRequest = new HttpServletRequestMock("false", "2", "12", null, null, null);
        List<Boolean> errors = formBean.checkDataRequest(mockRequest);
        TestCase.assertFalse(errors.contains(Boolean.FALSE));
    }
    
    @Test
    public void testFormBeanThree(){
        mockRequest = new HttpServletRequestMock("false", null, null, null, null, null);
        List<Boolean> errors = formBean.checkDataRequest(mockRequest);
        TestCase.assertFalse(errors.contains(Boolean.FALSE));
    }
    
    @Test
    public void testFormBeanFour(){
        mockRequest = new HttpServletRequestMock("pippoPalla", "2", "12", null, null, null);
        List<Boolean> errors = formBean.checkDataRequest(mockRequest);
        TestCase.assertTrue(errors.contains(Boolean.FALSE));
    }
    
    
}
